<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="wrapper">
    <div id="article">	
		<blockquote>
			<p>My dogs and cats love the whole sardines. :-)</p>
			<cite>CM &ndash; Santa Rosa, CA</cite>
		</blockquote>
        <blockquote>
            <p>Nora has had allergies and red, itchy skin for as long as I can remember.
            But since I've started her on a regiment of supplementing her diet with [Rara Avis] Healthy Powder 
            and have switched from fish oil to the Ecopawz frozen fish, 
            she has actually quit scratching!!!  
            And her skin is now a healthy pink instead of inflamed red.  
            You can't believe what a relief this is -- for both Nora and me.  
            Thank you, thank you, [Rara Avis] and Ecopawz!</p>
            <cite>GS &ndash; San Francisco, CA</cite>
        </blockquote>
        <blockquote>
            <p>I just wanted to share how pleased I am with my EcoPawz order and that I am looking forward to ordering again.
            <!--
               I purchased the ground halibut tubs, a sampler pack, and the whole sardines. My dogs love the sardines, although I hate to admit it but I have to cut them into bite-sized pieces because my prima dona wolfdog doesn't like to eat fish whole (snicker all you want - she deserves it - the big baby!) You get an average of 6 whole sardines in a bag, the sardines are approximately 8 inches in length and about two to three inches in diameter.
               I am spplementing the cats diet with the halibut grind. They love it and gobble it up as soon as I put the bowls down.
               -->
            </p> 
            <cite>AB &ndash; Tucson, AZ</cite>
        </blockquote>
        <blockquote>
        <p>
        The first thing people ask: "Is it really okay to feed fish?" I am so delighted to be able to suggest fish that I know are healthy and good for the animals. Thanks EcoPawz for your knowledge and expertise.
        </p>
        <cite>Pat McKay &ndash; Nevada <a href="http://www.animalhomeopathy.net">www.animalhomeopathy.net</a></cite>
        </blockquote>
<blockquote>
<p>I would like to order some of your fabulous new treats [Wiley's Turkey and Chicken Bark]! Even the picky dogs ate them! You were right...best work as of yet!</p>
<cite>Retailer - San Diego, CA </cite>
</blockquote>
	</div><!-- #article -->
</div><!-- #wrapper -->

