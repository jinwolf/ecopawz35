<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	EcoPawz::Handling &amp; Feeding
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container">
                   
                  <div class="title-1">
                   
                    <img src="<%=ResolveUrl("~/Content/images/1page-title2.gif") %>" alt="" />
                   
                  </div>
                  
                  <h1>Handling &amp; Feeding Instructions</h1>
              <h3>Our product is made of raw seafood � a great source of protein, calcium, vitamins and nutrients.</h3> 
              <p>The EcoPawz team supports variety in your pets diet, and we recommend using our dietary supplements for up to 25% of your companions overall needs and as treats or toppers for other natural pet food products. Check back with us often as we develop new items and recipes!

<div class="clear"></div>
<img src="<%=ResolveUrl("~/Content/images/dog_eyeing_fish.jpg")%>" class="fleft frame" alt="" /> 			  
<h3>Feeding Instructions</h3>
<p>
As with all changes in diet, please consult your veterinarian for feeding guidelines. 
</p><p>
Thaw product in refrigerator until completely thawed. 
Product can be warmed to room temperature, but do not leave out of refrigeration for any length of time. 
</p><p>
We recommend feeding up to 4oz of our product daily, depending upon your dogs dietary needs.  
For omega 3 maintenance, use 2oz of our product and reduce your regular food by 2oz and discontinue 
any fish oil supplements.  
</p><p>
For omega 3 therapy for dogs in need of larger amounts, feed 4 to 8 oz. daily and discontinue fish oil supplements. 
You know your animal best and feeding amounts and schedules should be adjusted according to age, activity level, 
and other pertinent medical conditions.
</p>
<div class="clear"></div>
<h3>Handling Instructions</h3>
<img src="<%=ResolveUrl("~/Content/images/4page-img4.jpg")%>" class="fleft" alt="" /> 
			 
<p>
This product is raw seafood. Store away from other foods and thoroughly clean all works surfaces,
 utensils and wash hands after use. 
 </p><p>
 Store product in freezer until ready to use and consult the feeding instructions.
</p><div class="clear"></div> 
            </div>    

</asp:Content>
