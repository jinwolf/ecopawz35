<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	EcoPawz::Wiley's Treats
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container">
                   
                  <div class="title-1">
                   
                    <img src="<%=ResolveUrl("~/Content/images/1page-title2.gif") %>" alt="" />
                   
                  </div>
                  
                  <h1>Grinds and Formulas</h1>
              <h3>EcoPawz line of raw food grinds and formulas are perfect for supplementing your pet's daily meal with a raw food boost. Our formulas are designed for an every-day balanced diet. Consisting primarily of meat bones and vitamin rich organ meat. Both EcoPawz grinds and formals are a great addition to your pet's raw diet. </h3> 

<img src="<%=ResolveUrl("~/Content/images/Grind_Duo_logo.jpg")%>" class="fleft frame" alt="" /> 

<p>Grinds and Formulas are available for the following:</p>
<ul>
<li>Turkey</li>
<li>Chicken</li>
<li>Duck</li>
<li>Salmon (Grind Only)</li>
<li>Sardine(Grind Only)</li>
</ul>
<p>EcoPawz grinds and formulas are available by the case. Please contact us today at <a href="#">info@ecopawz.com</a> for more information and to place an order.</p> 


              <div class="clear"></div> 
            </div>
             

</asp:Content>
