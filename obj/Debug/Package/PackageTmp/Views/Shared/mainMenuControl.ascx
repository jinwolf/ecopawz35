<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script runat="server">
    private string _currentPage;
    protected void Page_Init(object sender, EventArgs e)
    {

        _currentPage = string.Format("{0}_{1}",
                       ViewContext.RouteData.Values["controller"].ToString(),
                       ViewContext.RouteData.Values["id"].ToString());            
  
    }
    private string GetMenuItemClass(string menu)
    {
        if (string.Equals(_currentPage, menu, StringComparison.OrdinalIgnoreCase))
        {
            return "selected";

        }

        return "unselected";
    }

    private string IsSelected(string menu)
    {
        if (string.Equals(_currentPage, menu, StringComparison.OrdinalIgnoreCase))
        {
            return "selected";

        }

        return string.Empty;
    }    
</script>

<ul id="menu">
    <li class="<%=GetMenuItemClass("home_index") %>">                 
       <img class="littlePaw" src="<%= Url.Content("~/Content/images/little_paw" + IsSelected("home_index") + ".gif") %>" alt="little paw"/>
       <span class="menuItemBody"><a href="<%=Url.Content("~/") %>">Home</a></span>                                               
    </li>    
    <li class="<%=GetMenuItemClass("home_aboutus") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_aboutus") + ".gif") %>" alt="little paw"/>
       <span class="menuItemBody"><a href="<%=Url.Content("~/aboutus") %>">About Us</a></span>
    </li>
    <li class="<%=GetMenuItemClass("home_products") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_products") + ".gif") %>" alt="little paw"/>
        <span class="menuItemBody"><a href="http://www.shop.ecopawz.com/">Order Now</a></span>
    </li>
    <li class="<%=GetMenuItemClass("home_green") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_green") + ".gif") %>" alt="little paw"/>
          <span class="menuItemBody"><a href="<%=Url.Content("~/Green")%>">EcoPawz Green</a></span></li>
   <li class="<%=GetMenuItemClass("home_handling") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_handling") + ".gif") %>" alt="little paw"/>
        <span class="menuItemBody"><a href="<%=Url.Content("~/Handling")%>">Handling &amp; Feeding</a></span>
    </li>
    <li class="<%=GetMenuItemClass("home_benefitsofraw") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_benefitsofraw") + ".gif") %>" alt="little paw"/>
        <span class="menuItemBody"><a href="<%=Url.Content("~/BenefitsOfRaw")%>">Benefits of Raw</a></span></li>
    <li class="<%=GetMenuItemClass("home_faq") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_faq") + ".gif") %>" alt="little paw"/>
        <span class="menuItemBody"><a href="<%=Url.Content("~/faq")%>">FAQ</a></span></li>                        
    <li class="<%=GetMenuItemClass("home_links") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_links") + ".gif") %>" alt="little paw"/>
             <span class="menuItemBody"><a href="<%=Url.Content("~/Links")%>">The EcoPawz Pack</a></span>
        </li>
    <li class="<%=GetMenuItemClass("home_wileystreats") %>">
       <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_WileysTreats") + ".gif")%>" alt="little paw"/>
        <span class="menuItemBody"><a href="<%=Url.Content("~/WileysTreats")%>">Wiley's Treats</a></span></li>   
     <li class="<%=GetMenuItemClass("home_grindsandformulas") %>">
       <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_GrindsandFormulas") + ".gif")%>" alt="little paw"/>
         <span class="menuItemBody"><a href="<%=Url.Content("~/GrindsandFormulas")%>">Grinds and Formulas</a></span></li>     
    <li class="<%=GetMenuItemClass("home_distributorsandretailers") %>">
       <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_DistributorsandRetailers") + ".gif")%>" alt="little paw" style="margin-top:-30px"/>
         <span class="menuItemBody"><a href="<%=Url.Content("~/DistributorsandRetailers")%>">Distributors and Retailers</a></span></li>
    <li class="<%=GetMenuItemClass("home_contactus") %>">
        <img class="littlePaw" src="<%=Url.Content("~/Content/images/little_paw" + IsSelected("home_contactus") + ".gif") %>" alt="little paw"/>
        <span class="menuItemBody"><a href="<%=Url.Content("~/ContactUs")%>">Contact Us</a></span>
     </li>          
              
                
</ul>