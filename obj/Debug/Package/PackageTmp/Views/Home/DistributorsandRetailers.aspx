<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	EcoPawz::Contact Us
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container">
                   
                  <div class="title-1">
                   
                    <img src="<%=ResolveUrl("~/Content/images/1page-title2.gif") %>" alt="" />
                   
                  </div>
    <h1>EcoPawz for Distributors and Retailers</h1>
<p>All EcoPawz products are available for both distributors and retailers throughout the United States. We believe in developing long lasting relationships with our partners and look forward to getting to know you. Contact us today to receive more information on our products including Wiley's Treats, raw grinds and formulas.</p>



        <div class="wrapper"> 
            <div class="block"> 
            <div class="title-2">
                <!--
                    <img src="<%=ResolveUrl("~/content/images/7page-title3.gif") %>" alt="" />
                    -->                              
                <span style="font-weight:bold">Contact Details</span>
            </div>
            <h6>We're looking forward to hearing from you.</h6> 
            <p class="no-indent"> San Francisco, CA.<br /> 
                Phone:<em>(415) 685-3086</em> <br /> 
                E-mail: <a href="mailto:info@ecopawz.com">info@ecopawz.com</a> </p> 
         
            </div> 
         </div> 
        
 <div class="clear"></div> 
</div>
</asp:Content>
